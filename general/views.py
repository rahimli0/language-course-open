import datetime

from django.conf import settings
from django.shortcuts import render

# Create your views here.
from base_user.forms import LoginForm
from content.models import Partner, MainSystemInfo, LanguageCourse, Foundation, StaticPage


def base(req=None):

    data = {
        'now':datetime.datetime.now(),
        'base_site_name' : settings.SITE_NAME,
        'base_partners' : Partner.objects.filter(),
        'non_operate' : ['waiting','paided','ordered','closed','saled'],

        'login_form' : LoginForm(req.POST or None),
        'base_mainSystemInfo':MainSystemInfo.objects.filter().order_by('-created_date').first(),
        'base_language_courses' : LanguageCourse.objects.filter(),
        'base_foundations' : Foundation.objects.filter(),
        'base_static_pages' : StaticPage.objects.filter(parent=None),
        # 'base_categories':Menu.objects.filter(status=True,menu_type='dynamic-menu').filter(parent=None).order_by('order_index'),
        # 'base_categories':Menu.objects.filter(status=True,menu_type='dynamic-menu').filter(parent=None).order_by('order_index'),
    }
    return data

def base_auth(req=None):

    data = {
        'now':datetime.datetime.now(),
        'base_site_name' : settings.SITE_NAME,
        # 'base_categories':Menu.objects.filter(status=True,menu_type='dynamic-menu').filter(parent=None).order_by('order_index'),
        'base_profile' : req.user,
        'login_form' : LoginForm(req.POST or None),
        'non_operate' : ['waiting','paided','ordered','closed','saled'],
        'base_partners' : Partner.objects.filter(),
        'base_language_courses' : LanguageCourse.objects.filter(),
        'base_foundations' : Foundation.objects.filter(),
        'base_static_pages' : StaticPage.objects.filter(parent=None),
        'base_mainSystemInfo':MainSystemInfo.objects.filter().order_by('-created_date').first(),
    }
    return data

