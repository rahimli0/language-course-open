# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2020-01-25 12:53
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0011_auto_20200124_0327'),
    ]

    operations = [
        migrations.AddField(
            model_name='staticpage',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='content.StaticPage'),
        ),
    ]
