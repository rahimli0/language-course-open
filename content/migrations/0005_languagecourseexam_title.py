# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2020-01-14 23:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0004_auto_20200115_0322'),
    ]

    operations = [
        migrations.AddField(
            model_name='languagecourseexam',
            name='title',
            field=models.CharField(default='-', max_length=255),
        ),
    ]
