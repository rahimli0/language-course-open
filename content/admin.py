from django.contrib import admin

# Register your models here.
from hvad.admin import TranslatableAdmin

from content.models import *
from panel.models import *


@admin.register(LanguageCourseExam,ExamUser,AppealLanguageCourse,Appeal)
class GeneralAdmin(admin.ModelAdmin):
    pass

@admin.register(MainSystemInfo, WhyUs,EducationLanguage,EducationCountry,ProfessionSection,Profession,SuccessStory,Partner,ContactDepartments,MainSlider,HowWork,ExamLanguage)
class GeneralAdmin(TranslatableAdmin):
    pass

@admin.register(Article,LanguageCourse,Foundation,StaticPage,Program)
class ArticleAdmin(TranslatableAdmin):
    # list_display = ("title","date")
    prepopulated_fields = {'slug': ('unique_title',)}
    # list_filter = ("unique_name","start_date","end_date","date",)
    # list_editable = ("active",)
    # search_fields = ('unique_name','price',)
    # inlines = (OurProjectImageInlineAdmin,)

class AnswerInline(admin.TabularInline):
    model = Answer

@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    inlines = [
        AnswerInline,
    ]
