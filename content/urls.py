from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns

from .views import *


urlpatterns = [
	url(r'^blog/$', blog_list, name='blog-list'),
	url(r'^success-story-list/$', success_story_list, name='success-story-list'),
	url(r'^blog/deatil-(?P<id>\d+)/(?P<slug>[\w-]+)/$', blog_detail, name='blog-detail'),
	url(r'^language-courses/deatil-(?P<id>\d+)/(?P<slug>[\w-]+)/$', language_courses_datail, name='language-courses-detail'),

]
