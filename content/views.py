import datetime

from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404

# Create your views here.
# Create your views here.
from django.template.loader import render_to_string

from content.models import *
from general.views import base_auth
from django.utils.translation import ugettext as _
from panel.forms import AppealLanguageCourseForm


def blog_list(request):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    if request.method=='POST' and request.is_ajax():
        data_list = Article.objects.filter().order_by('-created_date')
        try:
            page_num = int(request.POST.get('page-num', 2))
        except:
            page_num = 0
        for_i = (page_num-1)*50

        # packages_list = packages[50*(page_num-1):50 * page_num]
        list_html = ''
        for data_list_item in data_list[50 * (page_num - 1):50 * page_num]:
            for_i += 1
            list_html = '{}{}'.format(list_html, render_to_string(
                "blog/include/item.html",
                {
                    'data_item': data_list_item,
                }
            ))
        main_html = list_html
        message_code = 1
        data = {
            'main_result': main_html,
            'message_code': message_code,
        }
        return JsonResponse(data=data)

    # context['homeMainForm'] =  HomeAppealForm(request.POST or None)
    # context['whyUses'] =  WhyUs.objects.filter()
    # context['teachers'] =  GUser.objects.filter(usertype=3)[:13]
    # context['successStories'] =  SuccessStory.objects.filter().order_by('-date')[:8]
    return render(request, 'blog/list.html', context=context)





def success_story_list(request):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    if request.method=='POST' and request.is_ajax():
        data_list = SuccessStory.objects.filter().order_by('-date')
        try:
            page_num = int(request.POST.get('page-num', 2))
        except:
            page_num = 0
        for_i = (page_num-1)*50

        # packages_list = packages[50*(page_num-1):50 * page_num]
        list_html = ''
        for data_list_item in data_list[50 * (page_num - 1):50 * page_num]:
            for_i += 1
            list_html = '{}{}'.format(list_html, render_to_string(
                "include/success-story-item.html",
                {
                    'data_item': data_list_item,
                }
            ))
        main_html = list_html
        message_code = 1
        data = {
            'main_result': main_html,
            'message_code': message_code,
        }
        return JsonResponse(data=data)

    # context['homeMainForm'] =  HomeAppealForm(request.POST or None)
    # context['whyUses'] =  WhyUs.objects.filter()
    # context['teachers'] =  GUser.objects.filter(usertype=3)[:13]
    # context['successStories'] =  SuccessStory.objects.filter().order_by('-date')[:8]
    return render(request, 'home/success-story.html', context=context)




def blog_detail(request,id,slug):
    now = datetime.datetime.now()
    context = base_auth(req=request)


    context['data_item'] =  get_object_or_404(Article,id=id,slug=slug)
    # context['whyUses'] =  WhyUs.objects.filter()
    # context['teachers'] =  GUser.objects.filter(usertype=3)[:13]
    # context['successStories'] =  SuccessStory.objects.filter().order_by('-date')[:8]
    return render(request, 'blog/detail.html', context=context)





def language_courses_datail(request,id,slug):
    now = datetime.datetime.now()
    context = base_auth(req=request)


    context['data_item'] =  get_object_or_404(LanguageCourse,id=id,slug=slug)

    form =  AppealLanguageCourseForm(request.POST)
    context['form'] = form

    if request.method == 'POST' and request.is_ajax():
        message_code = 0
        if form.is_valid():
            post = form.save(commit=False)
            post.course = context['data_item']
            post.save()
            message_code = 1
            message_text = _('Your reqquest succesfully added')
        else:
            message_text = str(form.errors)
        data = {
            'code':message_code,
            'text':message_text,
        }
        return JsonResponse(data)

    return render(request, 'pages/language-course-datail.html', context=context)

