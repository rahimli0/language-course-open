import re

from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.utils.translation import ugettext as _

from content.models import *


class MultiEmailField(forms.Field):
    def to_python(self, value):
        """Normalize data to a list of strings."""
        # Return an empty list if no input was given.
        if not value:
            return []
        return value.split(',')

    def validate(self, value):
        """Check if value consists only of valid emails."""
        # Use the parent's handling of required fields, etc.
        super().validate(value)
        for email in value:
            validate_email(email)

class PhoneNumberField(forms.CharField):
    def validate(self, value):
        """Check if value consists only of valid emails."""
        # Use the parent's handling of required fields, etc.
        super().validate(value)
        try:
            reg = re.findall(r"\(?\d{3}\)?[-.\s]?\d{3}[-.\s]?\d{4}", value)
            if len(reg) != 1:
                raise ValidationError(
                    _('Invalid phone number: %(value)s'),
                    code='invalid',
                    params={'value': '1'},
                )
        except:
            raise ValidationError(
                _('Invalid phone number: %(value)s'),
                code='invalid',
                params={'value': '1'},
            )
class HomeAppealForm(forms.Form):
    language = forms.ChoiceField(choices=[],label=_('Language'),  required=False, widget=forms.RadioSelect(attrs={'placeholder': _('Language'), 'autocomplete': 'off','class': 'custom-control-input', 'style':'width:100%'}))
    country = forms.ChoiceField(choices=[],label=_('Country'),  required=False, widget=forms.Select(attrs={'placeholder': _('Country'), 'autocomplete': 'off','class': 'rt-selectactive banner-select', 'style':'width:100%' }))
    profession = forms.ChoiceField(choices=[],label=_('Profession'),  required=False, widget=forms.Select(attrs={'placeholder': _('Profession'), 'autocomplete': 'off','class': 'rt-selectactive banner-select', 'style':'width:100%' }))
    profession_section = forms.ChoiceField(choices=[],label=_('Profession section'),  required=False, widget=forms.Select(attrs={'placeholder': _('Profession section'), 'autocomplete': 'off','class': 'rt-selectactive banner-select', 'style':'width:100%' }))
    def __init__(self, *args, **kwargs):
        super(HomeAppealForm, self).__init__(*args, **kwargs)

        try:
            self.fields['profession_section'].choices = [['', _('Profession Section')]] + [[x.id, x.title] for x in ProfessionSection.objects.filter()]
        except:
            pass
        try:
            self.fields['profession'].choices = [['', _('Profession')]] + [[x.id, x.title] for x in Profession.objects.filter()]
        except:
            pass
        try:
            self.fields['country'].choices = [['', _('Country')]] + [[x.id, x.title] for x in EducationCountry.objects.filter()]
        except:
            pass
        try:
            self.fields['language'].choices = [[x.id, x.title] for x in EducationLanguage.objects.filter()]
        except:
            pass



