from celery import shared_task


@shared_task
def send_html_mail(email,subject,html):
    from django.core.mail import send_mail
    from django.conf import settings
    send_mail(
        subject=subject,
        html_message=html,
        message='',
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[email],
        fail_silently=False
    )
