import uuid

from django.conf import settings
from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.urls import reverse
from hvad.models import TranslatableModel, TranslatedFields

from general.functions import path_and_rename, send_html_mail
from django.utils.translation import ugettext as _
from django.db.models import signals, Q


# Create your models here.
class MainSystemInfo(TranslatableModel):
    image = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    logo_white = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    logo_colored = models.ImageField(upload_to=path_and_rename, null=True, blank=True)

    phone = models.CharField(max_length=500, null=True, blank=True)
    email = models.EmailField(max_length=500, null=True, blank=True)

    certificate_count = models.IntegerField(default=0)
    partner_count = models.IntegerField(default=0)
    student_count = models.IntegerField(default=0)

    site_url = models.URLField("Url",max_length=255, null=True, blank=True)

    site_name = models.CharField("Site name",max_length=255, null=True, blank=True)
    translations = TranslatedFields(
        site_index_title_part1 = models.CharField(max_length=255, null=True, blank=True),
        site_index_title_part2 = models.CharField(max_length=255, null=True, blank=True),
        site_slogan = models.CharField("Site slogan",max_length=255),
        about_text = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True),
        short_title = models.CharField("Uzun slogan",max_length=255,blank=True,null=True,default=''),
        short_desc = models.CharField("Qısa təsvir",max_length=255,blank=True,null=True,default=''),
        youtube_video_url = models.URLField("Youtube video url",blank=True,null=True,default=''),
        desc = RichTextUploadingField("Təsvir",config_name='awesome_ckeditor',blank=True,null=True,default=''),
        meta_description = models.CharField(max_length=255,blank=True,null=True),
        meta_keywords = models.TextField(blank=True,null=True),

    )


    about_menu = models.BooleanField(default=True)
    language_course_menu = models.BooleanField(default=True)
    programs_menu = models.BooleanField(default=True)
    blog_menu = models.BooleanField(default=True)
    contact_menu = models.BooleanField(default=True)

    success_story_menu = models.BooleanField(default=True)
    how_does_works_menu = models.BooleanField(default=True)
    why_us_menu = models.BooleanField(default=True)

    certificates_section_menu = models.BooleanField(default=True)
    app_section_menu = models.BooleanField(default=True)
    video_section_menu = models.BooleanField(default=True)
    teacher_section_menu = models.BooleanField(default=True)
    partner_section_menu = models.BooleanField(default=True)

    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.site_name
    class Meta:
        verbose_name  = 'Əsas tənzimləmələr'
        verbose_name_plural  = 'Əsas tənzimləmələr'

class WhyUs(TranslatableModel):
    unique_title = models.CharField(max_length=255)
    translations = TranslatedFields(
        title = models.CharField(max_length=255),
        short_text = models.CharField(max_length=500),
        content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True),
    )
    image = models.ImageField(max_length=500,upload_to=path_and_rename, blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    class Meta:
        verbose_name  = 'Niyə biz bölməsi'
        verbose_name_plural  = 'Niyə biz bölmələri'


class HowWork(TranslatableModel):
    unique_title = models.CharField(max_length=255)
    translations = TranslatedFields(
        title = models.CharField(max_length=255),
        text = models.TextField(max_length=1000)
    )
    image = models.ImageField(max_length=500,upload_to=path_and_rename, blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    class Meta:
        verbose_name  = 'Sistem necə işləyir bölməsi'
        verbose_name_plural  = 'Sistem necə işləyir bölmələri'


class EducationLanguage(TranslatableModel):
    unique_title = models.CharField(max_length=255)
    translations = TranslatedFields(
        title = models.CharField(max_length=255),
        short_text = models.CharField(max_length=500),
        content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True),
    )
    image = models.ImageField(max_length=500,upload_to=path_and_rename, blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    class Meta:
        verbose_name  = 'Təhsil dili'
        verbose_name_plural  = 'Təhsil dilləri'



class LanguageCourse(TranslatableModel):
    unique_title = models.CharField(max_length=255)
    language = models.ForeignKey('EducationLanguage')
    translations = TranslatedFields(
        title = models.CharField(max_length=255),
        short_text = models.CharField(max_length=500),
        content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    )
    image = models.ImageField(max_length=500,upload_to=path_and_rename, blank=True,null=True)
    slug = models.SlugField()
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    class Meta:
        verbose_name  = 'Dil kursu'
        verbose_name_plural  = 'Dil kursları'

class Foundation(TranslatableModel):
    unique_title = models.CharField(max_length=255)
    country = models.ForeignKey('EducationCountry')
    translations = TranslatedFields(
        title = models.CharField(max_length=255),
        short_text = models.CharField(max_length=500),
        content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True),
    )
    image = models.ImageField(max_length=500,upload_to=path_and_rename, blank=True,null=True)
    slug = models.SlugField()
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    class Meta:
        verbose_name  = 'Department'
        verbose_name_plural  = 'Departmentlər'



class EducationCountry(TranslatableModel):
    unique_title = models.CharField(max_length=255)
    translations = TranslatedFields(
        title = models.CharField(max_length=255),
        short_text = models.CharField(max_length=500),
        content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True),
    )
    image = models.ImageField(max_length=500,upload_to=path_and_rename, blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    class Meta:
        verbose_name  = 'Təshsil verən ölkə'
        verbose_name_plural  = 'Təshsil verən ölkələr'



class Profession(TranslatableModel):
    unique_title = models.CharField(max_length=255)
    translations = TranslatedFields(
        title = models.CharField(max_length=255),
        short_text = models.CharField(max_length=500),
        content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    )
    image = models.ImageField(max_length=500,upload_to=path_and_rename, blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    class Meta:
        verbose_name  = 'İxtisas'
        verbose_name_plural  = 'İxtisaslar'



class ProfessionSection(TranslatableModel):
    unique_title = models.CharField(max_length=255)
    translations = TranslatedFields(
        title = models.CharField(max_length=255),
        short_text = models.CharField(max_length=500),
        content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    )
    image = models.ImageField(max_length=500,upload_to=path_and_rename, blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    class Meta:
        verbose_name  = 'İxtisas bölməsi'
        verbose_name_plural  = 'İxtisas bölmələri'



class  SuccessStory(TranslatableModel):
    unique_title = models.CharField(max_length=255)
    translations = TranslatedFields(
        title = models.CharField(max_length=255),
        short_title = models.CharField(max_length=10),
        content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    )
    image = models.ImageField(max_length=500,upload_to=path_and_rename, blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    class Meta:
        verbose_name  = 'Uğur hekayəsi'
        verbose_name_plural  = 'Uğur hekayələri'



class StaticPage(TranslatableModel):
    parent = models.ForeignKey('self',blank=True,null=True)
    unique_title = models.CharField(max_length=255)
    translations = TranslatedFields(
        title = models.CharField(max_length=255),
        short_title = models.CharField(max_length=500),
        content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True),
    )
    image = models.ImageField(max_length=500,upload_to=path_and_rename, blank=True,null=True)
    slug = models.SlugField(unique=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    def get_parents(self):
        return StaticPage.objects.filter(parent=self)
    class Meta:
        verbose_name  = 'Statik səhifə'
        verbose_name_plural  = 'Statik səhifələr'


class  Partner(TranslatableModel):
    unique_title = models.CharField(max_length=255)
    translations = TranslatedFields(
        title = models.CharField(max_length=255),
        short_text = models.CharField(max_length=500),
        link = models.URLField(blank=True,null=True),
        content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True),
    )
    image = models.ImageField(max_length=500,upload_to=path_and_rename, blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    class Meta:
        verbose_name  = 'Partnyor'
        verbose_name_plural  = 'Partnyorlar'


# Create your models here.
class Article(TranslatableModel):
    unique_title = models.CharField(max_length=255)
    author = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    image = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    translations = TranslatedFields(
        title = models.CharField("Başlıq",max_length=255),
        short_title = models.CharField("Qısa təsvir",max_length=255,blank=True,null=True,default=''),
        content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True),
        meta_description=models.TextField(blank=True, null=True),
        meta_keywords=models.TextField(blank=True, null=True),
    )
    view_count=models.IntegerField(default=0)
    slug = models.SlugField(default='-')
    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    class Meta:
        verbose_name  = 'Xəbər'
        verbose_name_plural  = 'Xəbərlər'


# Create your models here.
class MainSlider(TranslatableModel):
    unique_title = models.CharField(max_length=255)
    image = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    translations = TranslatedFields(
        title = models.CharField("Başlıq",max_length=255),
        content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True)
    )
    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    class Meta:
        verbose_name  = 'Əsas slayder'
        verbose_name_plural  = 'Əsas slayderlər'





class  ContactDepartments(TranslatableModel):
    unique_title = models.CharField(max_length=255)
    order_index = models.DecimalField(decimal_places=2,max_digits=5,default=20.0)
    translations = TranslatedFields(
        title = models.CharField(max_length=255),
        address = models.CharField(max_length=500)
    )
    phones = models.CharField(max_length=500)
    mobile = models.CharField(max_length=500)
    emails = models.CharField(max_length=500)
    map = models.TextField(blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    class Meta:
        verbose_name  = 'Department kontaktı'
        verbose_name_plural  = 'Departmentlərin kontaktları'





class ExamLanguage(TranslatableModel):
    unique_title = models.CharField(max_length=255)
    language = models.ForeignKey('EducationLanguage')
    translations = TranslatedFields(
        title = models.CharField(max_length=255),
        short_text = models.CharField(max_length=500),
        content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    )
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    class Meta:
        verbose_name  = 'Dil imtahanı'
        verbose_name_plural  = 'Dil imtahanları'


class Program(TranslatableModel):
    unique_title = models.CharField(max_length=255)
    image = models.ImageField(upload_to=path_and_rename)
    country = models.ForeignKey('content.EducationCountry',)
    language = models.ForeignKey('content.EducationLanguage',)
    profession = models.ForeignKey('content.Profession',)
    profession_section = models.ForeignKey('content.ProfessionSection',)
    translations = TranslatedFields(
        title = models.CharField("Başlıq",max_length=255),
        short_title = models.CharField("Qısa təsvir",max_length=255,blank=True,null=True,default=''),
        content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True),
        meta_description=models.TextField(blank=True, null=True),
        meta_keywords=models.TextField(blank=True, null=True),
    )
    slug = models.SlugField(unique=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.unique_title
    class Meta:
        verbose_name  = 'Proqram'
        verbose_name_plural  = 'Proqramlar'


class LanguageCourseExam(models.Model):
    language = models.ForeignKey('EducationLanguage')
    title = models.CharField(max_length=255)
    short_text = models.CharField(max_length=500)
    content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    image = models.ImageField(max_length=500,upload_to=path_and_rename, blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title
    class Meta:
        verbose_name  = 'Dil kursu imtahanı'
        verbose_name_plural  = 'Dil kursu imtahanları'

ExamUserCHOICE = (
    ('waiting',_('Waiting')),
    ('started',_('Started')),
    ('finished',_('Finished')),
)

class ExamUser(models.Model):
    uuid_id = models.UUIDField(_('uuid'), unique=True, default=uuid.uuid4)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.EmailField()
    phone = models.CharField(max_length=255)
    course = models.ForeignKey('content.LanguageCourseExam')
    status = models.CharField(max_length=255,choices=ExamUserCHOICE,default='waiting')
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.phone
    def get_full_name(self):
        return "{} {}".format(self.first_name,self.last_name)
    class Meta:
        verbose_name  = 'İmtahan verən istifadəçi'
        verbose_name_plural  = 'İmtahan verən istifadəçilər'

def exam_user_item_save(sender, instance, created, *args, **kwargs):
    if created:
        url = "http://{}{}".format('157.245.90.162',reverse('home:online-exam-detail', kwargs={'id':instance.uuid_id}))
        html = '<a href="{}">{}<a>'.format(url,_("Click here to start exam"))
        send_html_mail.delay(instance.email,_("Exam url"),html)
signals.post_save.connect(exam_user_item_save, sender=ExamUser)

class ExamUserQuestion(models.Model):
    question = models.ForeignKey('content.Question')
    exam_user = models.ForeignKey('content.ExamUser')
    answer = models.ForeignKey('content.Answer',blank=True,null=True)
    class Meta:
        verbose_name  = 'İmtahan sualı'
        verbose_name_plural  = 'İmtahan sualları'


class Question(models.Model):
    course = models.ForeignKey('content.LanguageCourseExam')
    content = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.content
    def get_answers(self):
        return Answer.objects.filter(question=self)
    class Meta:
        verbose_name  = 'Sual'
        verbose_name_plural  = 'Suallar'
class Answer(models.Model):
    question = models.ForeignKey('content.Question')
    result = models.BooleanField(default=False)
    content = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.content
    class Meta:
        verbose_name  = 'Cavab'
        verbose_name_plural  = 'Cavablar'