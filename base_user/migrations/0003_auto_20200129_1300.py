# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2020-01-29 09:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base_user', '0002_auto_20200125_1653'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myuser',
            name='usertype',
            field=models.IntegerField(blank=True, choices=[(1, 'Admin'), (4, 'Adminstrativ'), (2, 'Employee'), (3, 'Teacher')], null=True, verbose_name='User Type'),
        ),
    ]
