from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models

# Create your models here.
from hvad.models import TranslatedFields, TranslatableModel

from general.functions import path_and_rename




class Appeal(models.Model):
    first_name = models.CharField(max_length=255,blank=True,null=True,default='')
    last_name = models.CharField(max_length=255,blank=True,null=True,default='')
    email = models.EmailField(blank=True,null=True,default='')
    note = models.TextField(max_length=255,blank=True,null=True,default='')
    phone = models.CharField(max_length=255)
    country = models.ForeignKey('content.EducationCountry',)
    language = models.ForeignKey('content.EducationLanguage',)
    profession = models.ForeignKey('content.Profession',)
    profession_section = models.ForeignKey('content.ProfessionSection',)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.phone
    def get_full_name(self):
        return "{} {}".format(self.first_name,self.last_name)
    class Meta:
        verbose_name  = 'Təhsil üçün müraciət'
        verbose_name_plural  = 'Təhsil üçün müraciətlər'



class AppealLanguageCourse(models.Model):
    course = models.ForeignKey('content.LanguageCourse',)
    first_name = models.CharField(max_length=255,default='')
    last_name = models.CharField(max_length=255,default='')
    email = models.EmailField(blank=True,default='')
    phone = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.phone
    def get_full_name(self):
        return "{} {}".format(self.first_name,self.last_name)
    class Meta:
        verbose_name  = 'Kursa olan müraciət'
        verbose_name_plural  = 'Kursa olan müraciətlər'




class AppealProgram(models.Model):
    program = models.ForeignKey('content.Program',)
    first_name = models.CharField(max_length=255,default='')
    last_name = models.CharField(max_length=255,default='')
    email = models.EmailField(blank=True,default='')
    phone = models.CharField(max_length=255)
    note = models.TextField(blank=True,null=True,default='')
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.phone
    def get_full_name(self):
        return "{} {}".format(self.first_name,self.last_name)
    class Meta:
        verbose_name  = 'Proqrama olan müraciət'
        verbose_name_plural  = 'Proqrama olan müraciətlər'

