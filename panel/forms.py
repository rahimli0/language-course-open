from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _

from base_user.tools.common import USERTYPES
from content.forms import PhoneNumberField
from content.models import ExamUser
from panel.models import *

GUser = get_user_model()


ORDERTYPECHOICE  = (
    ('-', _('Desc')),
    ('',_('Asc')),
)

class AppealSearchForm(forms.Form):
    first_name = forms.CharField(max_length=255,label=_('First name'), required=False, widget=forms.TextInput(attrs={'placeholder': _('First name'), 'autocomplete': 'off','class': 'form-control', }))
    last_name = forms.CharField(max_length=255,label=_('Last name'), required=False, widget=forms.TextInput(attrs={'placeholder': _('Last name'), 'autocomplete': 'off','class': 'form-control', }))
    phone = forms.CharField(max_length=255,label=_('Phone'), required=False, widget=forms.TextInput(attrs={'placeholder': _('Phone'), 'autocomplete': 'off','class': 'form-control', }))
    country = forms.ChoiceField(label=_('Country'), required=False, choices=[], widget=forms.Select(attrs={'placeholder': _('Country'), 'autocomplete': 'off','class': 'form-control', }))
    language = forms.ChoiceField(label=_('Language'), required=False, choices=[], widget=forms.Select(attrs={'placeholder': _('Language'), 'autocomplete': 'off','class': 'form-control', }))
    profession = forms.ChoiceField(label=_('Profession'), required=False, choices=[], widget=forms.Select(attrs={'placeholder': _('Profession'), 'autocomplete': 'off','class': 'form-control', }))
    profession_section = forms.ChoiceField(label=_('Profession section'), required=False, choices=[], widget=forms.Select(attrs={'placeholder': _('Profession section'), 'autocomplete': 'off','class': 'form-control', }))
    search = forms.CharField(max_length=255,label=_('Search'), required=False, widget=forms.TextInput(attrs={'placeholder': _('Search'), 'autocomplete': 'off','class': 'form-control', }))
    field_list = forms.ChoiceField(choices=[],label=_('Field'),  required=True, widget=forms.Select(attrs={'placeholder': _('Field'), 'autocomplete': 'off','class': 'form-control', }))
    order_type = forms.ChoiceField(choices=ORDERTYPECHOICE ,label=_('Order'),  required=False, widget=forms.Select(attrs={'placeholder': _('Order'), 'autocomplete': 'off','class': 'form-control', }))
    def __init__(self, field_list_choice, *args, **kwargs):
        super(AppealSearchForm, self).__init__(*args, **kwargs)

        self.fields['field_list'].choices = field_list_choice

class AppealCourseSearchForm(forms.Form):
    first_name = forms.CharField(max_length=255,label=_('First name'), required=False, widget=forms.TextInput(attrs={'placeholder': _('First name'), 'autocomplete': 'off','class': 'form-control', }))
    last_name = forms.CharField(max_length=255,label=_('Last name'), required=False, widget=forms.TextInput(attrs={'placeholder': _('Last name'), 'autocomplete': 'off','class': 'form-control', }))
    phone = forms.CharField(max_length=255,label=_('Phone'), required=False, widget=forms.TextInput(attrs={'placeholder': _('Phone'), 'autocomplete': 'off','class': 'form-control', }))
    course = forms.ChoiceField(label=_('Language Course'), required=False, choices=[], widget=forms.Select(attrs={'placeholder': _('Language Course'), 'autocomplete': 'off','class': 'form-control', }))
    search = forms.CharField(max_length=255,label=_('Search'), required=False, widget=forms.TextInput(attrs={'placeholder': _('Search'), 'autocomplete': 'off','class': 'form-control', }))
    field_list = forms.ChoiceField(choices=[],label=_('Field'),  required=True, widget=forms.Select(attrs={'placeholder': _('Field'), 'autocomplete': 'off','class': 'form-control', }))
    order_type = forms.ChoiceField(choices=ORDERTYPECHOICE ,label=_('Order'),  required=False, widget=forms.Select(attrs={'placeholder': _('Order'), 'autocomplete': 'off','class': 'form-control', }))
    def __init__(self, field_list_choice, *args, **kwargs):
        super(AppealCourseSearchForm, self).__init__(*args, **kwargs)

        self.fields['field_list'].choices = field_list_choice



class GeneralSearchForm(forms.Form):
    search = forms.CharField(max_length=255,label=_('Search'), required=False, widget=forms.TextInput(
        attrs={'placeholder': _('Search'), 'autocomplete': 'off',
               'class': 'form-control', }))
    field_list = forms.ChoiceField(choices=[],label=_('Field'),  required=True, widget=forms.Select(attrs={'placeholder': _('Field'), 'autocomplete': 'off','class': 'form-control', }))
    order_type = forms.ChoiceField(choices=ORDERTYPECHOICE ,label=_('Order'),  required=False, widget=forms.Select(attrs={'placeholder': _('Order'), 'autocomplete': 'off','class': 'form-control', }))
    def __init__(self, field_list_choice, *args, **kwargs):
        super(GeneralSearchForm, self).__init__(*args, **kwargs)

        self.fields['field_list'].choices = field_list_choice




class EmployeeForm(forms.Form):
    name = forms.CharField(required=True,max_length=254, label="Name", error_messages={})
    surname = forms.CharField(required=True,max_length=254, label="Surname", error_messages={})
    email = forms.EmailField(required=True,error_messages={},label=_('Email'))
    usertype = forms.ChoiceField(choices=USERTYPES,required=True,error_messages={},label=_('Type'), widget=forms.Select(attrs={'placeholder': _("Type"),'class':'form-control'}))
    phone = PhoneNumberField(max_length=50,label=_('Phone'),  required=True, widget=forms.TextInput(attrs={'placeholder': _('Phone'), 'Language': 'off','class': 'form-control has-icon', 'style':'width:100%'}))
    password = forms.CharField(required=True, min_length=6,label=_("Password"))
    retype_password = forms.CharField(required=True,min_length=6,label=_("Password type"))


    def __init__(self, *args, **kwargs):
        super(EmployeeForm, self).__init__(*args, **kwargs)

        self.fields['name'].widget = forms.TextInput(attrs={
            'placeholder': _("Ad"),'class':'form-control'})
        self.fields['surname'].widget = forms.TextInput(attrs={
            'placeholder': _("Soyad"),'class':'form-control'})
        self.fields['email'].widget = forms.EmailInput(attrs={
            'placeholder': _("Email"),'class':'form-control'})
        self.fields['phone'].widget = forms.TextInput(attrs={
            'placeholder': _("Nömrə"),'class':'form-control'})
        self.fields['password'].widget = forms.PasswordInput(attrs={
            'placeholder': _("Şifrə"),'class':'form-control'})
        self.fields['retype_password'].widget = forms.PasswordInput(attrs={
            'placeholder': _("Şifrə"),'class':'form-control'})

    def clean(self):
        cleaned_data = super(EmployeeForm, self).clean()

        email = cleaned_data.get('email')
        password = cleaned_data.get('password')
        password_confirm = cleaned_data.get('retype_password')
        user_email_obj = GUser.objects.filter(email=email)
        if user_email_obj:
            self._errors['email'] = _('Email is allready use')
        if user_email_obj:
            self._errors['username'] = _('Email is allready use')
        if password and password_confirm:
            if password != password_confirm:
                raise forms.ValidationError(_("Passwords not same"))
        return cleaned_data





class EmployeeEditForm(forms.Form):
    name = forms.CharField(required=True,max_length=254, label="Ad", error_messages={})
    surname = forms.CharField(required=True,max_length=254, label="Soyad", error_messages={})
    email = forms.EmailField(required=True,error_messages={},label=_('Email'))
    usertype = forms.ChoiceField(choices=USERTYPES,required=True,error_messages={},label=_('Type'), widget=forms.Select(attrs={'placeholder': _("Type"),'class':'form-control'}))
    phone = forms.CharField(required=True,error_messages={},label=_('Nömrə'))
    password = forms.CharField(required=False,label=_("Şifrə"))
    retype_password = forms.CharField(required=False,label=_("Şifrə təsdiqi"))

    user_id = 0

    def __init__(self,user_id, *args, **kwargs):
        super(EmployeeEditForm, self).__init__(*args, **kwargs)
        self.user_id = user_id
        self.fields['name'].widget = forms.TextInput(attrs={'placeholder': _("Ad"),'class':'form-control'})
        self.fields['surname'].widget = forms.TextInput(attrs={'placeholder': _("Soyad"),'class':'form-control'})
        self.fields['email'].widget = forms.EmailInput(attrs={'placeholder': _("Email"),'class':'form-control'})
        self.fields['phone'].widget = forms.TextInput(attrs={'placeholder': _("Nömrə"),'class':'form-control'})
        self.fields['password'].widget = forms.PasswordInput(attrs={'placeholder': _("Şifrə"),'class':'form-control'})
        self.fields['retype_password'].widget = forms.PasswordInput(attrs={'placeholder': _("Şifrə təsdiqi"),'class':'form-control'})


    def clean(self):
        cleaned_data = super(EmployeeEditForm, self).clean()

        email = cleaned_data.get('email')
        password = cleaned_data.get('password')
        password_confirm = cleaned_data.get('retype_password')

        user_email_obj = GUser.objects.filter(email=email).exclude(id=self.user_id)
        if user_email_obj:
            self._errors['email'] = _('Email is allready use')
        if user_email_obj:
            self._errors['username'] = _('Email is allready use')
        if password:
            if password and password_confirm:
                if password != password_confirm:
                    raise forms.ValidationError(_("Passwords not same"))
        return cleaned_data


class AppealProgramModelForm(forms.ModelForm):

    class Meta:
        model = AppealProgram
        fields = ('first_name',
                  'last_name',
                  'email',
                  'note',
                  'phone',
                  'program',
                  )
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control','placeholder': _("First name"), }),
            'last_name': forms.TextInput(attrs={'class': 'form-control','placeholder': _("Last name"), }),
            'phone': forms.TextInput(attrs={'class': 'form-control','placeholder': _("Phone"), }),
            'email': forms.TextInput(attrs={'class': 'form-control','placeholder': _("Email"), }),
            'note': forms.Textarea(attrs={'class': 'form-control','placeholder': _("Last name"), }),
            'program': forms.Select(attrs={'class': 'form-control', 'placeholder': _("Program"), }),
        }
class AppealModelForm(forms.ModelForm):

    class Meta:
        model = Appeal
        fields = ('first_name',
                  'last_name',
                  'email',
                  'note',
                  'phone',
                  'country',
                  'language',
                  'profession',
                  'profession_section',
                  )
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control','placeholder': _("First name"), }),
            'last_name': forms.TextInput(attrs={'class': 'form-control','placeholder': _("Last name"), }),
            'phone': forms.TextInput(attrs={'class': 'form-control','placeholder': _("Phone"), }),
            'email': forms.TextInput(attrs={'class': 'form-control','placeholder': _("Email"), }),
            'note': forms.Textarea(attrs={'class': 'form-control','placeholder': _("Last name"), }),
            'profession': forms.Select(attrs={'class': 'form-control', 'placeholder': _("Profession"), }),
            'country': forms.Select(attrs={'class': 'form-control', 'placeholder': _("Country"), }),
            'language': forms.Select(attrs={'class': 'form-control', 'placeholder': _("Language"), }),
            'profession_section': forms.Select(attrs={'class': 'form-control', 'placeholder': _("Profession section"), }),
        }
class AppealLanguageCourseForm(forms.ModelForm):

    class Meta:
        model = AppealLanguageCourse
        fields = ('first_name',
                  'last_name',
                  'email',
                  'phone',
                  )
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
        }
class AppealProgramForm(forms.ModelForm):

    class Meta:
        model = AppealProgram
        fields = ('first_name',
                  'last_name',
                  'email',
                  'phone',
                  )
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control' }),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
        }

class ExamUserModelForm(forms.ModelForm):

    class Meta:
        model = ExamUser
        fields = ('first_name',
                  'last_name',
                  'email',
                  'phone',
                  'course',
                  )
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control','placeholder': _("First name"), }),
            'last_name': forms.TextInput(attrs={'class': 'form-control','placeholder': _("Last name"), }),
            'phone': forms.TextInput(attrs={'class': 'form-control','placeholder': _("Phone"), }),
            'email': forms.TextInput(attrs={'class': 'form-control','placeholder': _("Mail"), }),
            'course': forms.Select(attrs={'class': 'form-control', 'placeholder': _("Profession"), }),
        }
