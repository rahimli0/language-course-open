# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2020-01-15 04:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('panel', '0002_appeallanguagecourse'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appeallanguagecourse',
            name='email',
            field=models.EmailField(blank=True, default='', max_length=254),
        ),
        migrations.AlterField(
            model_name='appeallanguagecourse',
            name='first_name',
            field=models.CharField(default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='appeallanguagecourse',
            name='last_name',
            field=models.CharField(default='', max_length=255),
        ),
    ]
