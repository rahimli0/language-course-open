import hashlib
import random

from celery.utils.time import timezone
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.db.models import Q
from datetime import datetime
from django.http import JsonResponse, Http404
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse

from content.forms import HomeAppealForm
from panel.models import Appeal, AppealLanguageCourse, AppealProgram

GUser = get_user_model()
from general.views import base_auth
from panel.forms import GeneralSearchForm, EmployeeForm, EmployeeEditForm, AppealSearchForm, AppealModelForm, \
    AppealCourseSearchForm, AppealLanguageCourseForm, AppealProgramForm, AppealProgramModelForm

from django.utils.translation import ugettext as _
# Create your views here.

# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------


EMPLOYEECHOICE = (
    (1, _("Admin")),
    (4, _("Adminstrativ")),
    (2, _("Employee")),
    (3, _("Teacher")),
)

@login_required(login_url='base-user:login')
def dashboard(request):

    context = base_auth(req=request)

    if request.method == 'POST' and request.is_ajax():
        message_code = 0
        result_data = ''
        # try:
        result_data = '{}'.format(render_to_string(
            "panel/include/home/main.html",
            {
                'appeals_count': Appeal.objects.filter().count(),
                'appeal_language_courses_count': AppealLanguageCourse.objects.filter().count(),
                'employees_count': GUser.objects.filter(usertype=2,is_active=True).count(),
            }
        ))
        message_code = 1
        # except:
        #     pass
        data = {
            'main_result': result_data,
            'message_code': message_code,
        }
        return JsonResponse(data=data)

    return render(request, 'panel/dashboard.html', context=context)

USERSearchCHOICE = (
    ('first_name',_('First name')),
    ('last_name',_('Last name')),
    # ('date',_('Date')),
)

@login_required(login_url='base-user:login')
def user_list(request):

    context = base_auth(req=request)
    inital_data = {
        'order_type':'-',
        'field_list':'title',
    }
    search_form = GeneralSearchForm(USERSearchCHOICE,request.POST or None,initial=inital_data)
    if request.method=='POST' and request.is_ajax():
        if search_form.is_valid():
            cleaned_data = search_form.cleaned_data
            first_name = cleaned_data.get('first_name',None)
            last_name = cleaned_data.get('last_name',None)
            field_list = cleaned_data.get('field_list',None)
            order_type = cleaned_data.get('order_type',None)
            data_list = GUser.objects.filter().order_by('{}{}'.format(order_type,field_list))
    #         gender = cleaned_data.get('gender',None)
    #         show_photo = cleaned_data.get('show_photo',None)
    #         _list_item_html = ''
            if first_name:
                data_list = data_list.filter(first_name__icontains=first_name)

                data_list = data_list.filter(first_name__icontains=last_name)
            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0
            list_html = ''
            for_i = (page_num-1)*50
            data_list = data_list[50 * (page_num - 1):50 * page_num]
            for data_list_item in data_list[50 * (page_num - 1):50 * page_num]:
                for_i += 1
                list_html = '{}{}'.format(list_html, render_to_string(
                    "panel/users/include/list-item.html",
                    {
                        'for_i': for_i,
                        'data_item': data_list_item,
                    }
                ))
            if page_num < 2:
                main_html = "{}".format(
                    render_to_string(
                        'panel/users/include/main.html',
                        {
                            'list_html': list_html,
                        }
                    )
                )
            else:
                main_html = list_html
            message_code = 1
            data = {
                'main_result': main_html,
                'message_code': message_code,
            }
            return JsonResponse(data=data)
    context['search_form'] = search_form


    return render(request, 'panel/users/user-list.html', context=context)


@login_required(login_url='base-user:login')
def user_edit(request,id):
    user = request.user
    if user.usertype == 2:
        raise Http404
    context = base_auth(req=request)
    user_obj = get_object_or_404(GUser,id=id)
    inital_data = {
        'name':user_obj.first_name,
        'surname':user_obj.last_name,
        'email':user_obj.email,
        'phone':user_obj.phone,
    }
    employee_form = EmployeeEditForm(id,request.POST or None, request.FILES or None,initial=inital_data)


    context = base_auth(req=request)
    if request.method == 'POST' and request.is_ajax():
        message_code = 0
        if employee_form.is_valid():
            clean_data = employee_form.cleaned_data
            name = clean_data.get('name',None)
            surname = clean_data.get('surname',None)
            email = clean_data.get('email',None)
            phone = clean_data.get('phone',None)
            usertype = clean_data.get('usertype',None)
            password = clean_data.get('password')

            random_string = str(random.random()).encode('utf8')
            salt = hashlib.sha1(random_string).hexdigest()[:5]

            user_obj.first_name=name
            user_obj.last_name=surname
            user_obj.email=email
            user_obj.username=email
            user_obj.usertype=usertype
            user_obj.phone=phone
            if password:
                password = make_password(password, salt=salt)
                user_obj.password=password
            user_obj.save()
            message_code = 1
            data = {
                'message_code':message_code,
                'message_title': _('Uğurla qeydə alındı'),
                'message_text': '',
                'redirect_url': reverse('panel:user-list', kwargs={}),
            }
        else:
            message_title = _('Düzgün məlumat daxil edin')
            message_text = "{}".format(str(employee_form.errors))
            data = {
                'message_code': message_code,
                'message_title': message_title,
                'message_text': message_text,
            }
        return JsonResponse(data=data)
            # employee_form = EmployeeEditForm(user_id=user_obj.id)
        # else:
        #     return HttpResponse(employee_form.errors)

    context['form'] = employee_form
    context['user_obj'] = user_obj
    response = render(request, 'panel/users/user-form.html', context=context)
    return response




@login_required(login_url='base-user:login')
def user_create(request):
    now = datetime.now()
    user = request.user
    if user.usertype == 2:
        raise Http404
    context = base_auth(req=request)
    employee_form = EmployeeForm(request.POST or None, request.FILES or None)
    context['employee_form'] = employee_form


    context = base_auth(req=request)
    if request.method == "POST" and request.is_ajax():
        message_code = 0
        if employee_form.is_valid():
            clean_data = employee_form.cleaned_data
            name = clean_data.get('name')
            surname = clean_data.get('surname')
            email = clean_data.get('email')
            phone = clean_data.get('phone')
            usertype = clean_data.get('usertype')
            password = clean_data.get('password')
            random_string = str(random.random()).encode('utf8')
            salt = hashlib.sha1(random_string).hexdigest()[:5]
            password = make_password(str(password).strip(), salt=salt)

            user_obj = GUser(first_name=name,
                             last_name=surname,
                             email=email,
                             username=email,
                             phone=phone,
                             password=password,
                             usertype=usertype,
                             is_active=True,
             )
            user_obj.save()
            message_code = 1
            data = {
                'message_code':message_code,
                'message_title': _('Uğurla qeydə alındı'),
                'message_text': '',
                'redirect_url': reverse('panel:user-list', kwargs={}),
            }
        else:

            message_title = _('Düzgün məlumat daxil edin')
            message_text = "{}".format(str(employee_form.errors))
            data = {
                'message_code': message_code,
                'message_title': message_title,
                'message_text': message_text,
            }
        return JsonResponse(data=data)

    context['form'] = employee_form
    response = render(request, 'panel/users/user-form.html', context=context)
    return response

AppealSearchCHOICE = (
    ('first_name',_('First name')),
    ('last_name',_('Last name')),
    ('date',_('Date')),
)

def appeal_list(request):

    context = base_auth(req=request)
    inital_data = {
        'order_type':'-',
        'field_list':'title',
    }
    search_form = AppealSearchForm(AppealSearchCHOICE,request.POST or None,initial=inital_data)
    inital_data = {'order_type':'-'}
    if request.method=='POST' and request.is_ajax():
        if search_form.is_valid():
            cleaned_data = search_form.cleaned_data
            search_text = cleaned_data.get('search',None)
            field_list = cleaned_data.get('field_list',None)
            order_type = cleaned_data.get('order_type',None)
            data_list = AppealProgram.objects.filter().order_by('{}{}'.format(order_type,field_list))
            if search_text:
                data_list = data_list.filter(Q(note__icontains=search_text))
            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0
            for_i = (page_num-1)*50

            # packages_list = packages[50*(page_num-1):50 * page_num]
            list_html = ''
            for data_list_item in data_list[50 * (page_num - 1):50 * page_num]:
                for_i += 1
                list_html = '{}{}'.format(list_html, render_to_string(
                    "panel/appeals/include/list-item.html",
                    {
                        'for_i': for_i,
                        'data_item': data_list_item,
                    }
                ))
            if page_num < 2:
                main_html = "{}".format(
                    render_to_string(
                        'panel/appeals/include/main.html',
                        {
                            'list_html': list_html,
                        }
                    )
                )
            else:
                main_html = list_html
            message_code = 1
            data = {
                'main_result': main_html,
                'message_code': message_code,
            }
            return JsonResponse(data=data)
    context['search_form'] = search_form
    # context['title'] = title
    # context['type'] = type


    return render(request, 'panel/appeals/appeals-list.html', context=context)


# def appeal_create(request):
#     now = datetime.now()
#     user = request.user
#     context = base_auth(req=request)
#     form = HomeAppealForm(request.POST or None, request.FILES or None)
#     if request.method == "POST" and request.is_ajax():
#         message_code = 0
#         if form.is_valid():
#             clean_data = form.cleaned_data
#             phone = clean_data.get('phone')
#             language = clean_data.get('language')
#             country = clean_data.get('country')
#             profession = clean_data.get('profession')
#             profession_section = clean_data.get('profession_section')
#
#             user_obj = AppealProgram(phone=phone,
#                               language_id=language,
#                               country_id=country,
#                               profession_id=profession,
#                               profession_section_id=profession_section,
#              )
#             user_obj.save()
#             message_code = 1
#             data = {
#                 'message_code':message_code,
#                 'message_title': _('Your reqquest succesfully added'),
#                 'message_text':  _('Your reqquest succesfully added. will give responnse to you'),
#             }
#         else:
#
#             message_title = _('Düzgün məlumat daxil edin')
#             message_text = "{}".format(str(form.errors))
#             data = {
#                 'message_code': message_code,
#                 'message_title': message_title,
#                 'message_text': message_text,
#             }
#         return JsonResponse(data=data)
#     else:
#         raise Http404



@login_required(login_url='base-user:login')
def appeal_view(request,id):
    user = request.user
    context = base_auth(req=request)
    data_obj = get_object_or_404(AppealProgram,id=id)

    context = base_auth(req=request)
    if request.method == 'POST' and request.is_ajax():
        form = AppealProgramModelForm(request.POST, instance=data_obj)
        message_code = 0
        if form.is_valid():
            post = form.save(commit=False)
            # post.author = request.user
            message_code = 1
            post.save()
            data = {
                'message_code':message_code,
                'message_title': _('Succesfully changed'),
                'message_text': '',
                'redirect_url': reverse('panel:appeal-view', kwargs={'id':data_obj.id}),
            }
        else:
            message_title = _('Düzgün məlumat daxil edin')
            message_text = "{}".format(str(form.errors))
            data = {
                'message_code': message_code,
                'message_title': message_title,
                'message_text': message_text,
            }
        return JsonResponse(data=data)
    else:
        form = AppealProgramModelForm(instance=data_obj)
    context['data_obj'] = data_obj
    context['form'] = form

    response = render(request, 'panel/appeals/view.html', context=context)
    return response



# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------



def appeal_language_course_list(request):

    context = base_auth(req=request)
    inital_data = {
        'order_type':'-',
        'field_list':'title',
    }
    search_form = AppealCourseSearchForm(AppealSearchCHOICE,request.POST or None,initial=inital_data)
    inital_data = {'order_type':'-'}
    if request.method=='POST' and request.is_ajax():
        if search_form.is_valid():
            cleaned_data = search_form.cleaned_data
            search_text = cleaned_data.get('search',None)
            field_list = cleaned_data.get('field_list',None)
            order_type = cleaned_data.get('order_type',None)
            data_list = AppealLanguageCourse.objects.filter().order_by('{}{}'.format(order_type,field_list))
            if search_text:
                data_list = data_list.filter(Q(first_name__icontains=search_text) | Q(last_name__icontains=search_text) | Q(email__icontains=search_text) | Q(phone__icontains=search_text))
            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0
            for_i = (page_num-1)*50

            # packages_list = packages[50*(page_num-1):50 * page_num]
            list_html = ''
            for data_list_item in data_list[50 * (page_num - 1):50 * page_num]:
                for_i += 1
                list_html = '{}{}'.format(list_html, render_to_string(
                    "panel/appeals-course/include/list-item.html",
                    {
                        'for_i': for_i,
                        'data_item': data_list_item,
                    }
                ))
            if page_num < 2:
                main_html = "{}".format(
                    render_to_string(
                        'panel/appeals-course/include/main.html',
                        {
                            'list_html': list_html,
                        }
                    )
                )
            else:
                main_html = list_html
            message_code = 1
            data = {
                'main_result': main_html,
                'message_code': message_code,
            }
            return JsonResponse(data=data)
    context['search_form'] = search_form
    # context['title'] = title
    # context['type'] = type


    return render(request, 'panel/appeals-course/list.html', context=context)




@login_required(login_url='base-user:login')
def appeal_language_course_view(request,id):
    user = request.user
    context = base_auth(req=request)
    data_obj = get_object_or_404(AppealLanguageCourse,id=id)

    context = base_auth(req=request)
    if request.method == 'POST' and request.is_ajax():
        form = AppealLanguageCourseForm(request.POST, instance=data_obj)
        message_code = 0
        if form.is_valid():
            post = form.save(commit=False)
            # post.author = request.user
            message_code = 1
            post.save()
            data = {
                'message_code':message_code,
                'message_title': _('Succesfully changed'),
                'message_text': '',
                'redirect_url': reverse('panel:appeal--language-course-view', kwargs={'id':data_obj.id}),
            }
        else:
            message_title = _('Düzgün məlumat daxil edin')
            message_text = "{}".format(str(form.errors))
            data = {
                'message_code': message_code,
                'message_title': message_title,
                'message_text': message_text,
            }
        return JsonResponse(data=data)
    else:
        form = AppealLanguageCourseForm(instance=data_obj)
    context['data_obj'] = data_obj
    context['form'] = form

    response = render(request, 'panel/appeals-course/view.html', context=context)
    return response

























