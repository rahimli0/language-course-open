from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns

from .views import *


urlpatterns = [

    url(r'^$', dashboard, name='dashboard'),
    url(r'^user/list/$', user_list, name='user-list'),
    url(r'^user/create/', user_create, name='user-create'),
    url(r'^user/edit/(?P<id>[0-9]+)/', user_edit, name='user-edit'),

    url(r'^appeal/list/$', appeal_list, name='appeal-list'),
    url(r'^appeal/view/(?P<id>[0-9]+)/', appeal_view, name='appeal-view'),

    url(r'^appeal-language-course/list/$', appeal_language_course_list, name='appeal-language-course-list'),
    url(r'^appeal-language-course/view/(?P<id>[0-9]+)/', appeal_language_course_view, name='appeal--language-course-view'),
]
