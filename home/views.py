import base64
import copy
import datetime
import hashlib
import operator
import random
from functools import reduce

from django.contrib.auth import get_user_model, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.forms import formset_factory, inlineformset_factory
from django.http import JsonResponse, Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from dateutil import relativedelta as rdelta
# Create your views here.
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone

from content.forms import HomeAppealForm
from content.models import *
from general.views import base_auth
from django.utils.translation import ugettext as _

from django.contrib import messages

from panel.forms import *

GUser = get_user_model()

from django.shortcuts import render

from general.views import base


def error_404(request):
    context = base(req=request)
    return render(request, '404.html', context)
# Create your views here.
def index(request):
    now = timezone.now()
    context = base_auth(req=request)


    context['homeMainForm'] =  HomeAppealForm(request.POST or None)
    context['whyUses'] =  WhyUs.objects.filter()
    context['howWorks'] =  HowWork.objects.filter()
    context['teachers'] =  GUser.objects.filter(usertype=3)[:13]
    context['successStories'] =  SuccessStory.objects.filter().order_by('-date')[:8]
    context['articles'] =  Article.objects.filter().order_by('-created_date')[:6]
    context['main_slider'] =  MainSlider.objects.filter().order_by('-created_date')[:6]
    return render(request, 'home/home.html', context=context)


# Create your views here.
def about(request):
    now = timezone.now()
    context = base_auth(req=request)
    context['teachers'] =  GUser.objects.filter(usertype=3)[:13]


    # context['homeMainForm'] =  HomeAppealForm(request.POST or None)
    context['whyUses'] =  WhyUs.objects.filter()
    # context['teachers'] =  GUser.objects.filter(usertype=3)[:13]
    # context['successStories'] =  SuccessStory.objects.filter().order_by('-date')[:8]
    return render(request, 'home/about.html', context=context)




# Create your views here.
def search(request):
    now = timezone.now()
    context = base_auth(req=request)


    context['homeMainForm'] = HomeAppealForm(request.POST or None)
    if request.method=='POST' and request.is_ajax() and context['homeMainForm'].is_valid():
        data_list = Program.objects.filter().order_by('-date')
        try:
            page_num = int(request.POST.get('page-num', 2))
        except:
            page_num = 0
        for_i = (page_num-1)*50
        cleaned_data = context['homeMainForm'].cleaned_data
        country = cleaned_data.get('language',None)
        language = cleaned_data.get('language',None)
        profession = cleaned_data.get('profession',None)
        profession_section = cleaned_data.get('profession_section',None)
        if country:
            data_list = data_list.filter(country=country)
        if language:
            data_list = data_list.filter(language=language)
        if profession:
            data_list = data_list.filter(profession=profession)
        if profession_section:
            data_list = data_list.filter(profession_section=profession_section)
        list_html = ''
        for data_list_item in data_list[50 * (page_num - 1):50 * page_num]:
            for_i += 1
            list_html = '{}{}'.format(list_html, render_to_string(
                "home/program/include/item.html",
                {
                    'data_item': data_list_item,
                }
            ))
        main_html = list_html
        message_code = 1
        data = {
            'main_result': main_html,
            'message_code': message_code,
        }
        return JsonResponse(data=data)
    else:
        inital_data = {}
        if request.GET.get('language'):
            inital_data.update({'language':request.GET.get('language')})
        if request.GET.get('country'):
            inital_data.update({'country':request.GET.get('country')})
        if request.GET.get('profession'):
            inital_data.update({'profession':request.GET.get('profession')})
        if request.GET.get('profession_section'):
            inital_data.update({'profession_section':request.GET.get('profession_section')})
        context['homeMainForm'] = HomeAppealForm(request.POST or None,initial=inital_data)
    return render(request, 'home/program/search.html', context=context)



def program_detail(request,id,slug):
    now = datetime.datetime.now()
    context = base_auth(req=request)


    context['data_item'] =  get_object_or_404(Program,id=id,slug=slug)

    form =  AppealProgramForm(request.POST)
    context['form'] = form

    if request.method == 'POST' and request.is_ajax():
        message_code = 0
        if form.is_valid():
            post = form.save(commit=False)
            post.program = context['data_item']
            post.save()
            message_code = 1
            message_text = _('Your reqquest succesfully added')
        else:
            message_text = str(form.errors)
        data = {
            'code':message_code,
            'text':message_text,
        }
        return JsonResponse(data)
    # context['whyUses'] =  WhyUs.objects.filter()
    # context['teachers'] =  GUser.objects.filter(usertype=3)[:13]
    # context['successStories'] =  SuccessStory.objects.filter().order_by('-date')[:8]
    return render(request, 'home/program/details.html', context=context)





# Create your views here.
def contact(request):
    now = timezone.now()
    context = base_auth(req=request)
    context['teachers'] =  GUser.objects.filter(usertype=3)[:13]


    # context['homeMainForm'] =  HomeAppealForm(request.POST or None)
    context['contact_uses'] =  ContactDepartments.objects.filter().order_by('order_index')
    # context['teachers'] =  GUser.objects.filter(usertype=3)[:13]
    # context['successStories'] =  SuccessStory.objects.filter().order_by('-date')[:8]
    return render(request, 'home/contact.html', context=context)




# Create your views here.
def online_exam(request):
    now = timezone.now()
    context = base_auth(req=request)
    context['teachers'] =  GUser.objects.filter(usertype=3)[:13]


    # context['homeMainForm'] =  HomeAppealForm(request.POST or None)
    exam_form =  ExamUserModelForm(request.POST)

    if request.method == 'POST' and request.is_ajax():
        message_code = 0
        if exam_form.is_valid():
            post = exam_form.save(commit=False)
            post.save()
            message_code = 1
            message_text = _('Your reqquest succesfully added')
        else:
            message_text = str(exam_form.errors)
        data = {
            'code': message_code,
            'text': message_text,
        }
        return JsonResponse(data)

    context['exam_form'] = exam_form
    # context['teachers'] =  GUser.objects.filter(usertype=3)[:13]
    # context['successStories'] =  SuccessStory.objects.filter().order_by('-date')[:8]
    return render(request, 'home/online-exam.html', context=context)


# Create your views here.
def online_exam_detail(request,id):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    item = get_object_or_404(ExamUser,uuid_id=id,status='waiting')
    if request.method=='POST' and request.is_ajax():
        item.status = 'started'
        item.save()
        data_list =  Question.objects.filter(course=item.course).order_by('?')[:20]
        list_html = ''
        list_data = []
        for_i = 0
        for data_list_item in data_list:
            for_i += 1
            ExamUserQuestion.objects.filter(exam_user=item).delete()
            data_item = ExamUserQuestion(question=data_list_item,exam_user=item)
            list_data.append(data_item)
            list_html = '{}{}'.format(list_html, render_to_string(
                "home/exam/include/item.html",
                {
                    'data_item': data_list_item,
                    'for_i': for_i,
                    'count': data_list.count(),
                }
            ))
        ExamUserQuestion.objects.bulk_create(list_data)
        main_html = list_html
        message_code = 1
        data ={
            'code':message_code,
            'result':main_html,
        }
        return JsonResponse(data)
    context['data_item'] = item
    return render(request, 'home/exam/exam-detail.html', context=context)


# Create your views here.
def online_exam_detail_check(request,id):
    now = datetime.datetime.now()
    item = get_object_or_404(ExamUser,uuid_id=id,status='started')
    if request.method=='POST' and request.is_ajax():
        true_count = 0
        false_count = 0
        message_code = 0
        data_list = ExamUserQuestion.objects.filter(exam_user=item)
        for data_list_item in data_list:
            try:
                qa = int(request.POST.get('answerGroup{}'.format(data_list_item.question_id)))
                data_list_item.answer_id = qa
                data_list_item.save()
                if data_list_item.answer.result:
                    true_count += 1
                else:
                    false_count += 1
            except:
                pass
        message_code = 1
        item.status = 'finished'
        item.save()
        data ={
            'code':message_code,
            'result':"<p style='color: green;font-weight: 700;'>{}: {}<p>"
                     "<p style='color: red;font-weight: 700;'>{}: {}<p>".format(_("Correct count"),true_count,_("Wrong count"),false_count),
        }
        return JsonResponse(data)



# Create your views here.
def static_page(request,slug):
    now = timezone.now()
    context = base_auth(req=request)
    context['data_item'] =  get_object_or_404(StaticPage,slug=slug)
    return render(request, 'home/page.html', context=context)

