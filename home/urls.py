from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns

from .views import *


urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^about/$', about, name='about'),
	# url(r'^error_404/$', error_404, name='error_404'),
	url(r'^programs/$', search, name='search'),
	url(r'^programs/deatil-(?P<id>\d+)/(?P<slug>[\w-]+)/$', program_detail, name='program-detail'),
	url(r'^contact/$', contact, name='contact-us'),
	url(r'^online-exam/$', online_exam, name='online-exam'),
	url(r'^online-exam/detail/(?P<id>[\w-]+)/$', online_exam_detail, name='online-exam-detail'),
	url(r'^online-exam/detail/(?P<id>[\w-]+)/check/$', online_exam_detail_check, name='online-exam-detail-check'),
	url(r'^page/(?P<slug>[\w-]+)/$', static_page, name='static-page'),

]
