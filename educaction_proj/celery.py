import os

from celery import Celery, shared_task
from celery.schedules import crontab
import datetime

import requests

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'educaction_proj.settings')

app = Celery('educaction_proj')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))

app.conf.timezone = 'Asia/Baku'
# @app.task



app.conf.beat_schedule = {
    # "see-you-in-ten-seconds-task": {
    #     "task": "educaction_proj.celery.see_youso",
    #     "schedule": 10.0,
    #     "args": [],
    # },
    # "daily-dask-date-in-twenty-seconds-task": {
    #     "task": "educaction_proj.celery.active_waiting_daily_dask",
        # "schedule": 250.0,
        # "schedule": 3.0,
        # 'schedule': datetime.timedelta(seconds=400), # 300 = every 5 minutes
        # "schedule": crontab(minute='*/5'),
        # "schedule": crontab(hour=0, minute=35),
        # "schedule": crontab(hour=0, minute=19),
        # "args": [],
    # },
}